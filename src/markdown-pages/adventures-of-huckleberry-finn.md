---
# src/markdown-pages/adventures-of-huckleberry-finn.md
my-id: "adventures-of-huckleberry-finn"
title: "Adventures of Huckleberry Finn"
author:
  id: "mark-twain"
---

<p><b>The Adventures of Huckleberry Finn</b>, is a novel by American author Mark Twain, which was first published in the United Kingdom in December 1884 and in the United States in February 1885.</p>
