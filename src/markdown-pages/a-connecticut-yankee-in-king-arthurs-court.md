---
# src/markdown-pages/a-connecticut-yankee-in-king-arthurs-court.md
my-id: "a-connecticut-yankee-in-king-arthurs-court"
title: "A Connecticut Yankee in King Arthur's Court"
author:
  id: "mark-twain"
---

<p><b>A Connecticut Yankee in King Arthur's Court</b> is an 1889 novel by American humorist and writer Mark Twain. The book was originally titled A Yankee in King Arthur's Court. Some early editions are titled A Yankee at the Court of King Arthur.
