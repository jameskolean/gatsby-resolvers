import * as React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"

const IndexPage = ({ data }) => (
  <Layout>
    <div>
      <h1>
        Welcome to <b>Resolver Example</b>
      </h1>
      {
        data.allDataJson.nodes.map((author) =>
          <div>
            <h4>{author.name}</h4>
            {author.books.referenced.map((book) =>
              <p>{book.frontmatter.title}</p>
            )}
          </div>
        )
      }
    </div>
  </Layout>
)

export default IndexPage

export const pageQuery = graphql`
  query AuthorQuery {
    allDataJson {
      nodes {
        books {
          referenced {
            frontmatter {
              title
            }
            html
          }
        }
        name
      }
    }
  }
`