// gatsby-node.js
const {
    genericArrayResolver,
    genericResolver,
} = require("./gatsby/resolvers");
exports.createResolvers = ({ createResolvers }) => {
    const resolvers = {
        ...genericResolver("MarkdownRemarkFrontmatterAuthor", "DataJson"),
        ...genericArrayResolver("DataJsonBooks", "MarkdownRemark"),
    }
    createResolvers(resolvers);
}