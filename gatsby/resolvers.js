// gatsby/resolvers.js

const genericResolver = function genericResolver(nodeType, targetNodeType) {
    return {
        // this is the parent node type
        [nodeType]: {
            // add a new node called referenced to add the target to.
            referenced: {
                type: targetNodeType,
                async resolve(source, args, context, info) {
                    // bailout if there is no id
                    if (!source.id) {
                        return null
                    }
                    return await context.nodeModel.findOne({
                        query: {
                            filter: {
                                my_id: {
                                    in: source.id,
                                },
                            },
                        },
                        type: targetNodeType,
                    })
                },
            },
        },
    }
}

const genericArrayResolver = function genericArrayResolver(nodeType, targetNodeType) {
    return {
        // this is the parent node type
        [nodeType]: {
            // add a new node called referenced to add the target to.
            referenced: {
                type: [targetNodeType],
                async resolve(source, args, context, info) {
                    // bailout if there is no id
                    if (!source.id || source.lenght === 0) {
                        return []
                    }
                    // selecting one at a time to maintain order
                    const result = []
                    for (const id of source.id) {
                        const node = await context.nodeModel.findOne({
                            query: {
                                filter: {
                                    // this is a hack due to the example construction
                                    // normally the ids will not be nested and you can delete `frontmatter`
                                    frontmatter: {
                                        my_id: {
                                            eq: id,
                                        },
                                    },
                                },
                            },
                            type: targetNodeType,
                        })
                        result.push(node)
                    }
                    return result
                },
            },
        },
    }
}
module.exports = {
    genericArrayResolver,
    genericResolver,
}
