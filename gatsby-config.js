module.exports = {
  siteMetadata: {
    title: `Gatsby Resolvers `,
    description: `Shows off custom Gatsby resolvers.`,
    author: `jameskolean@gmail.com`,
    author: `jameskolean@gmail.com`,
    siteUrl: `https://jameskolean.gitlab.io`,
  },
  pathPrefix: `/gatsby-resolvers`,
  plugins: [
    `gatsby-transformer-json`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/data/`,
      },
    },
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `markdown-pages`,
        path: `${__dirname}/src/markdown-pages`,
      }
    }
  ]
}
